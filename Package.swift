// swift-tools-version:5.0

import PackageDescription

let package = Package(
    name: "NetMind",
    platforms: [
        .macOS(.v10_12), .iOS(.v10), .watchOS(.v4), .tvOS(.v10)
    ],
    products: [        
        .library(name: "NetMind", targets: ["NetMind"])
    ],
    dependencies: [

    ],
    targets: [
        .target(name: "NetMind", dependencies: []),
        .testTarget(name: "NetMindTests", dependencies: ["NetMind"]),
    ]
)
